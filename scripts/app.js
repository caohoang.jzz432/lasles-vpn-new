$('.customer__list').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: true,
  prevArrow:
    "<button type='button' class='slick-prev pull-left'><img src='./images/arrow-back.svg' /></button>'",
  nextArrow:
    "<button type='button' class='slick-next pull-left'><img src='./images/arrow-next.svg' /></button>'",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
        dots: true,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
      },
    },
  ],
});

const menuToggle = document.querySelector('.menu__toggle');
const menu = document.querySelector('.menu');

menuToggle.addEventListener('click', function () {
  menu.classList.add('is-show');
});
window.addEventListener('click', function (e) {
  if (!menu.contains(e.target) && !e.target.matches('.menu__toggle')) {
    menu.classList.remove('is-show');
  }
});
